#!/usr/bin/env bash


##### Configure #####

cd /home/vagrant/app
# Create tables
php fwc createtables

# Fill database
php fwc parse
