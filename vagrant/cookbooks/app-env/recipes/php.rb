package "php5-common"
package "php5-fpm"
package "php5-cli"
package "php5-cgi"

package "php-pear"
package "make"
package "php5-curl"
package "php5-mysql"
package "imagemagick"
package "php5-imagick"
package "php5-dev"


bash "set-timezone" do
  user "root"
  code <<-EOH
    sed -i 's/;date.timezone.*/date.timezone = "UTC"/g' /etc/php5/fpm/php.ini
    sed -i 's/;date.timezone.*/date.timezone = "UTC"/g' /etc/php5/cli/php.ini
    service php5-fpm restart
  EOH
end

bash "disable-expose" do
  user "root"
  code <<-EOH
    sed -i 's/expose_php.*/expose_php = Off/g' /etc/php5/fpm/php.ini
    sed -i 's/expose_php.*/expose_php = Off/g' /etc/php5/cli/php.ini
    service php5-fpm restart
  EOH
end

bash "install-php-unit" do
  user "root"
  returns [0, 1]
  code <<-EOH
    pear channel-discover pear.phpunit.de
    pear channel-discover pear.symfony.com
    pear install pear.phpunit.de/PHPUnit
    pear install phpunit/DbUnit
    pear install phpunit/PHP_Invoker
    pear install phpunit/PHPUnit_Selenium
    pear install phpunit/PHPUnit_Story
  EOH
end
