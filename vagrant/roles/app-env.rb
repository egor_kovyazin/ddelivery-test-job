name "app-env"
description "Base role applied to app node"
override_attributes(
)
default_attributes(
)
run_list(
    "recipe[apt::default]",
    "recipe[app-env::mc]",
    "recipe[app-env::unzip]",
    "recipe[app-env::git]",
    "recipe[app-env::nginx]",
    "recipe[app-env::mysql]",
    "recipe[app-env::php]",
)
