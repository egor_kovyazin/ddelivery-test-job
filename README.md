Тестовое задание для кандита в PHP-программисты ddelivery.ru
============================

СТРУКТУРА ПРОЕКТА
------------------

      commands/               консольные команды
      config/                 конфиги
      controllers/            контроллеры
      core/                   ядро мини-фреймворка
      
          Application        базовый класс прилоежния
          ClassLoader        загрузчик классов
          Command            базовый класс команды
          ConsoleApplication консольное приложение
          Controller         базовый класс контроллера
          Fw                 базовый класс фреймворка (аналог Yii)
          WebApplication     веб-приложение
      
      models/                модели
      runtime/               логи приложения
      vagrant/               настройки VAGRANT
      views/                 вьюшки
      web/                   публичные скрипты
      
      fwc                    консольные команды


ЗАВИСИМОСТИ
------------

Полный список зависимостей можно посмотреть по рецептам Chef в vagrant/cookbooks/recipes.
Кратко список такой:

    * nginx
    * mysql v5.5
    * PHP v5.5.9 (fpm, curl, pdo, mysqli)


ЗАПУСК ПОД VAGRANT-ом
---------------------

### Необходимо установить на хост-машине

    * VirtualBox >= 4.3.10 and Extension Pack
    * Vagrant >= 1.7.2
    * Account on https://atlas.hashicorp.com (без аккаунта у вас не зальется Box)
    * Прописать в hosts '171.15.37.10	ddelivery-test.local'


### Быстрый старт

    git clone git@bitbucket.org:egor_kovyazin/ddelivery-test-job.git app && cd app \
    && git submodule sync && git submodule init && git submodule update \
    && cd vagrant \
    && vagrant plugin install vagrant-omnibus && vagrant plugin install vagrant-vbguest && vagrant up
    
    echo -e '\n# Test project for ddelivery\n171.15.37.10 ddelivery-test.local' >> /etc/hosts


Когда Vagrant успешно подымется (завершится vagrant up), проект будет доступен в браузере по адресу http://ddelivery-test.local

Для доступа к базе можно использовать любой клиент на хост-машине.

    * Адрес: 171.15.37.10:3306 ddelivery ddelivery
    * Логин / пасс: ddelivery / ddelivery


О ФРЕЙМОРКЕ И ПРИЛОЖЕНИИ
------------------------

Фремйворк Fw - это самописная штука, а точнее: сильно упрощенный Yii v1 (написан специально для тестового задания).


### Что есть

* маршрутизация запросов
* консольные команды, например:

```
#!bash
# Создание таблицы items
php fwc createtables --verbose
```

```
#!bash
# Парсинг CSV файла, приложенного к заданию
php fwc parse --verbose [--csvfile=data/test.csv]
```


САМО ТЕСТОВОЕ ЗАДАНИЕ
---------------------

1. Разобрать данные из файла test.csv, вытащить максимально возможное количество данных и записать в mysql-таблицу со следующей структурой:
      * Раздел каталога
      * Подраздел каталога
      * Артикул товара
      * Бренд
      * Модель
      * Наименование товара
      * Доступные размеры
      * Доступные цвета
      * Ориентация право \ лево (для клюшек)

1. Сделать простенький интерфейс для вывода информации из полученной базы