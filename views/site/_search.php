<?php
/** @var array $filter */

use controllers\SiteController;
use models\Items;

$sections    = Items::getInstance()->getSectionsList();
$brands      = Items::getInstance()->getBrandsList();

?>

<form class="form-horizontal" role="form" method="get" action="/">

	<div class="form-group">
		<label class="control-label col-sm-2" for="items[model]">Model:</label>
		<div class="col-sm-5">
			<input class="form-control" id="items[model]" name="items[model]" placeholder="Enter model">
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-sm-2" for="items[section]">Section:</label>
		<div class="col-sm-5">
			<select class="form-control" id="items[section]" name="items[section]">
				<option value="">...</option>
				<?php foreach ($sections as $section): ?>
					<?php if (empty($section['title'])): ?>
					<option value="<?= SiteController::NONE_VALUE ?>">-none- (<?= $section['quantity'] ?>)</option>
					<?php else: ?>
					<option value="<?= $section['title'] ?>"><?= $section['title'] ?> (<?= $section['quantity'] ?>)</option>
					<?php endif; ?>
				<?php endforeach; ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-sm-2" for="items[brand]">Brand:</label>
		<div class="col-sm-5">
			<select class="form-control" id="items[brand]" name="items[brand]">
				<option value="">...</option>
				<?php foreach ($brands as $brand): ?>
					<?php if (empty($brand['title'])): ?>
					<option value="<?= SiteController::NONE_VALUE ?>">-none- (<?= $brand['quantity'] ?>)</option>
					<?php else: ?>
					<option value="<?= $brand['title'] ?>"><?= $brand['title'] ?> (<?= $brand['quantity'] ?>)</option>
					<?php endif; ?>
				<?php endforeach; ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-sm-2" for="items[direction]">Direction:</label>
		<div class="col-sm-5">
			<select class="form-control" id="items[direction]" name="items[direction]">
				<option value="">...</option>
				<option value="l">Левая</option>
				<option value="r">Правая</option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-5">
			<button type="submit" class="btn btn-primary">Search</button>
			<button type="reset" class="btn btn-default">Reset</button>
		</div>
	</div>
</form>

