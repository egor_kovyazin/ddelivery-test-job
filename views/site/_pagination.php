<?php
/**
 * @var integer $currentPage
 * @var integer $itemsOnPage
 * @var integer $itemsCount
 */

$uri = preg_replace('/&page=[0-9]+|page=[0-9&]+/', '', $_SERVER['REQUEST_URI']);
if (strrpos($uri, '?') == strlen($uri) - 1) {
	 $uri = str_replace('?', '', $uri);
}

$maskedUri = $uri . (strpos($uri, '?') ? '&': '?') . 'page={{page}}';
$pagesCount = ceil($itemsCount / $itemsOnPage);

if ($pagesCount < 10) {
	$range = range(1, $pagesCount);
	$pages = array_combine($range, $range);
} else {
	$borderMin = max($currentPage - 3, 1);
	$borderMax = min($currentPage + 3, $pagesCount);

	$range = range($borderMin, $borderMax);
	$pages = array_combine($range, $range);
	$pages[1] = 1;
	$pages[$pagesCount] = $pagesCount;

	if (!isset($pages[2])) {
		$pages[2] = null;
	}
	if (!isset($pages[$pagesCount-1])) {
		$pages[$pagesCount-1] = null;
	}

	ksort($pages);
}
?>

<?php if (count($pages) > 1): ?>
<ul class="pagination">
	<?php foreach($pages as $number => $value): ?>
	<li <?= $number == $currentPage ? ' class="active"': '' ?>>
		<a href="<?= $value || $number == $currentPage ? str_replace('{{page}}', $value, $maskedUri): '#' ?>"><?= $value ?: '...' ?></a>
	</li>
	<?php endforeach; ?>
</ul>
<?php endif; ?>
