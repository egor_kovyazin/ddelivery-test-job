<?php
/**
 * @var controllers\SiteController $this
 * @var array   $items
 * @var integer $itemsCount
 * @var integer $currentPage
 * @var integer $itemsOnPage
 */
?>

<?= $this->renderPartial('_search', []); ?>

<table class="table table-striped">
	<thead>
		<tr>
			<th>ID</th>
			<th>Brand</th>
			<th>Section</th>
			<th>Subsection</th>
			<th>Article</th>
			<th>Model</th>
			<th>Size</th>
			<th>Color</th>
			<th>Direction</th>
			<th>Title</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($items as $item): ?>
		<tr>
			<td><?= $item['id'] ?></td>
			<td><?= $item['brand'] ?></td>
			<td><?= $item['section'] ?></td>
			<td><?= $item['subsection'] ?></td>
			<td><?= $item['article'] ?></td>
			<td><?= $item['model'] ?></td>
			<td><?= $item['size'] ?></td>
			<td><?= $item['color'] ?></td>
			<td><?= $item['direction'] ?></td>
			<td><?= $item['title'] ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<?= $this->renderPartial('_pagination', [
	'currentPage' => $currentPage,
	'itemsOnPage' => $itemsOnPage,
	'itemsCount'  => $itemsCount,
]); ?>