<?php
/**
 * Основной конфигурационный файл приложения.
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * @uses web/index.php | fwc
 */
return [
	// Путь до корня проекта.
	'basePath' => __DIR__ . '/..',

	'components' => [

		/**
		 * Настройки подключения через PDO
		 * @uses \core\Application::__construct()
		 */
		'db' => [
			'connectionString' => 'mysql:host=127.0.0.1;dbname=ddelivery;charset=utf8',
			'username'         => 'ddelivery',
			'password'         => 'ddelivery',
			'charset'          => 'utf8',
		],

	],
];

