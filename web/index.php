<?php
/**
 * Стертер web-приложения.
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */


error_reporting(E_ALL & ~E_NOTICE);

$root = __DIR__ . '/..';

require_once($root . '/core/autoload.php');

// Загрузка ядря
require_once($root . '/core/Fw.php');

// Загрузка конфига
$config = require_once($root . '/config/main.php');

// Инициализация web-приложения
$app = \core\Fw::createWebApplication($config);

// Запуск приложения
$app->run();
