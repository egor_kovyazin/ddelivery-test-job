<?php

namespace commands;

use core\Fw;
use core\Command;
use PDOException;

class CreatetablesCommand extends Command
{

	/**
	 * Создание таблицы "items"
	 */
	public function actionIndex()
	{
		$sql = "CREATE TABLE `items` ("
		. "`id`         int(11) NOT NULL AUTO_INCREMENT,"
		. "`section`    varchar(255) DEFAULT NULL,"
		. "`subsection` text DEFAULT '',"
		. "`article`    varchar(255) DEFAULT '',"
		. "`brand`      varchar(255) DEFAULT '',"
		. "`model`      varchar(255) DEFAULT '',"
		. "`title`      varchar(255) DEFAULT '',"
		. "`size`       varchar(255) DEFAULT '',"
		. "`color`      varchar(255) DEFAULT '',"
		. "`direction`  varchar(1) DEFAULT '',"
		. "PRIMARY KEY (`id`))";

		try {
			Fw::app()->db->exec($sql);
			$this->log("Table \"$table\" created.");
		} catch(PDOException $e) {
			$this->log($e->getMessage());
		}
	}
}
