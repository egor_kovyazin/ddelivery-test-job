<?php

namespace commands;

use core\Fw;
use core\Command;

class ParseCommand extends Command
{
	public function actionIndex($csvfile = null)
	{
		$csvfile = file_exists($csvfile) ? $csvfile: Fw::getBasePath() . DIRECTORY_SEPARATOR . '/data/test.csv';

		if (($handle = fopen($csvfile, 'r')) === false) {
			$this->log("Не удалось открыть файл \"$csvfile\"");
		}
		$this->log("...Разбор файла \"$csvfile\"...");

		$data = $this->getPreparedData($handle);
		$this->processData($data);

		fclose($handle);

		$this->log("...Финал...");
	}

	private function processData($dataMap)
	{
		$this->truncateTableItems();

		foreach ($dataMap as $brand => $data) {

			foreach ($data as $section => $items) {

				foreach ($items as $item) {
					$item['brand']   = $brand;
					$item['section'] = $section;
					unset($item['prepared']);
					$this->saveItem($item);

					$this->log('-- Saved item: ' . var_export($item, true));
				}
			}
		}
	}

	private function truncateTableItems()
	{
		$this->log('Truncate table "items"');
		try {
			Fw::app()->db->exec('TRUNCATE TABLE `items`');
		} catch(\PDOException $e) {
			$this->log($e->getMessage());
		}
	}

	private function saveItem($item)
	{
		$item['subsection'] = join('|', $item['subsections']);
		unset($item['subsections']);

		$item['color'] = join('|', $item['colors']);
		unset($item['colors']);

		$fieldsList = join(', ', array_keys($item));
		$params = str_repeat("?, ", count($item)-1) . '?';
		$sql = "INSERT INTO items ($fieldsList) VALUES ($params)";

		try {
			$query = Fw::app()->db->prepare($sql);
			$query->execute(array_values($item));
		} catch(\PDOException $e) {
			$this->log($e->getMessage());
		}
	}

	private function getPreparedData($resource)
	{
		$data = [];

		while (($row = fgets($resource, 1000)) !== false) {

			$row = str_replace(["\n", "\r\n"], '', $row);
			$row = preg_replace('/\D"|^"/', '', $row);
			$row = mb_strtolower(trim($row), 'UTF-8');
			$row = preg_replace('/[ ]{2,}/', ' ', $row);

			$preparedStr = $row;

			// Определение бренда
			$brand = $this->prepareBrand($preparedStr);

			// Определение раздела
			$section = $this->prepareSection($preparedStr);

			// Определение артикула
			preg_match('/\w*[0-9-]{6,}/', $preparedStr, $matches);
			$article = trim(array_shift($matches));
			$preparedStr = str_replace($article, '', $preparedStr);

			// Определение подраздела
			$subsections = $this->prepareSubsection($preparedStr);

			// Определение цветов
			$colors = $this->prepareColors($preparedStr);

			// Определение ориентации клюшки
			$direction = null;
			if ($section == 'клюшка') {
				preg_match('/[ -]+(r|l)$/', $preparedStr, $matches);
				$direction = trim(array_shift($matches), ' -');
				$preparedStr = str_replace($direction, '', $preparedStr);
			}

			// Определение размера
			$size = $this->prepareSizes($preparedStr);

			// Чистка от скобок и двойных пробелов.
			$preparedStr = trim($preparedStr, ' ,-.');
			$preparedStr = str_replace('/', '', $preparedStr);
			$preparedStr = preg_replace('/\(\s*\)/', '', $preparedStr);
			$preparedStr = preg_replace('/[ ]{2,}/', ' ', trim($preparedStr, ' -'));

			if (!isset($data[$brand])) {
				$data[$brand] = [];
			}

			if (!isset($data[$brand][$section])) {
				$data[$brand][$section] = [];
			}

			$data[$brand][$section][$row] = [
				'prepared'    => $preparedStr,
				'subsections' => $subsections,
				'article'     => $article,
				'title'       => $row,
				'model'       => $preparedStr,
				'colors'      => $colors,
				'size'        => $size,
				'direction'   => $direction,
			];
		}

		return $data;
	}

	private function prepareBrand(&$str)
	{
		$reg = '/' . join('|', $this->brands) . '/';
		preg_match($reg, $str, $matches);
		$brand = trim(array_shift($matches));
		$str = str_replace($brand, '', $str);
		return (string)$brand;
	}

	private function prepareSection(&$str)
	{
		$reg = '/' . join('|', $this->sections) . '/';
		preg_match($reg, $str, $matches);
		$section = trim(array_shift($matches));
		$str = str_replace($section, '', $str);
		return isset($this->sectionSynonims[$section]) ? $this->sectionSynonims[$section]: (string)$section;
	}

	private function prepareSubsection(&$str)
	{
		$subsections = [];
		foreach ($this->subsections as $subsection) {
			preg_match('/' . $subsection . '/', $str, $matches);

			if (empty($matches)) continue;

			$subsection = trim(array_shift($matches));
			$str = str_replace($subsection, '', $str);
			$subsections[] = isset($this->subsectionSynonims[$subsection]) ? $this->subsectionSynonims[$subsection]: $subsection;
		}
		return $subsections;
	}

	private function prepareColors(&$str)
	{
		$colors = [];
		foreach ($this->shades as $shade => $regShade) {
			foreach ($this->colors as $color => $regColor) {
				preg_match('/' . $regShade . $regColor . '/', $str, $matches);
				$detectedColor = trim(array_shift($matches));

				if (!empty($detectedColor)) {
					$colors[] = $shade . $color;
					$str = str_replace($detectedColor, '', $str);
				}
			}
		}

		foreach ($this->colors as $color => $regColor) {
			preg_match('/' . $regColor . '/', $str, $matches);
			$detectedColor = trim(array_shift($matches));

			if (!empty($detectedColor)) {
				$colors[] = $color;
				$str = str_replace($detectedColor, '', $str);
			}
		}
		return $colors;
	}

	private function prepareSizes(&$str)
	{
		preg_match('/[ -]+([0-9.,]+|[lmsx]+[=0-9]*|reg|frt)( см|"| [()de]+|\W)*$/', $str, $matches);
		$size = trim(array_shift($matches), ' -,');
		if (floatval($size) < 500) {
			$str = str_replace($size, '', $str);
			return str_replace(',', '.', $size);
		}

		return null;
	}

	private $sections = [
		'белье',
		'блин',
		'блокер',
		'брюки',
		'визор',
		'гамаши',
		'гаситель',
		'запасные винты',
		'защита',
		'защитная решетка на шлем',
		'камень',
		'клюшк[аи]+',
		'коньки',
		'корректор заточного диска',
		'костюм',
		'кофта',
		'крепеж',
		'крюк',
		'куртка',
		'лезвие',
		'ловушка',
		'лопасть',
		'майка',
		'маска',
		'нагрудник',
		'налокотник',
		'паховая защита',
		'перчатки',
		'подтяжки',
		'поло',
		'пояс',
		'раковина',
		'регулятор жесткости конька',
		'ремень',
		'рубашка',
		'рукоятка',
		'сандали',
		'свитер',
		'сетка для хоккейных ворот',
		'стакан',
		'сумка',
		'толстовка',
		'трубка',
		'трусы',
		'форма',
		'футболка',
		'чехлы',
		'чехол',
		'шайба',
		'шлем',
		'шнурки',
		'шорты',
		'щитки',
	];

	private $sectionSynonims = [
		'клюшки' => 'клюшка',
		'чехлы'  => 'чехол',
	];

	private $subsections = [
		'алмазный',
		'без лезвия',
		'в сборе',
		'ветрозащитный',
		'ветронепроницаемая',
		'вратарск[ийеая]+',
		'вратаря',
		'голени',
		'горла',
		'дер',
		'деревянная',
		'для гамаш',
		'для заточки коньков',
		'для коньков',
		'для профессиональной хоккейной сетки',
		'для ринк-бенди',
		'для трусов',
		'для шлема',
		'для[0-9 х-]*клюшек',
		'женская',
		'запястья',
		'игровой',
		'игрока',
		'к лезвиям',
		'композитная',
		'композитный',
		'лица',
		'локтя',
		'на колесах',
		'на молнии',
		'нижнее',
		'плеч',
		'с капюшоном',
		'с клипсами',
		'с колесами',
		'с композитным крюком',
		'с маской',
		'с петлями',
		'с подкладкой',
		'с подтяжками',
		'с раковиной',
		'спортивн[ыйоеая]+',
		'тренеровочн[ыйоеая]+',
		'утепленн[ыйоеая]+',
		'флисовая',
		'хоккейн[ыеоая]+',
		'шеи',
	];

	private $subsectionSynonims = [
		'ветронепроницаемая' => 'ветрозащитный',
		'вратарская'    => 'вратарские',
		'вратаря'       => 'вратарские',
		'вратарский'    => 'вратарские',
		'дер'           => 'деревянная',
		'композитная'   => 'композитный',
		'с колесами'    => 'на колесах',
		'спортивная'    => 'спортивный',
		'спортивное'    => 'спортивный',
		'тренеровочная' => 'тренеровочный',
		'хоккейное'     => 'хоккейная',
		'хоккейные'     => 'хоккейная',
	];

	private $brands = [
		'bauer', 'ccm', 'easton', 'graf', 'gufex', 'jofa',
		'koho', 'kosa', 'mad guy', 'mission', 'montreal', 'nbh', 
		'pallas', 'rbk','reebok', 'sher-wood', 'tac', 'torspo', 'tps',
	];

	private $colors = [
		'белый'   => 'бел[ыйоеая]*',
		'желтый'  => 'желт[ыйоеая]*',
		'зеленый' => 'зелен[ыйоеая]*',
		'золотой' => 'золот[ойеая]*',
		'красный' => 'крас[ныйоеая]*',
		'синий'   => 'син[ийея]*',
		'серый'   => 'сер[ыйоеая]*',
		'хром'    => 'хром',
		'черный'  => 'черн[ыйоеая]*',
	];

	private $shades = [
		'бело-'   => 'бело[-\/]+',
		'желто-'  => 'желто[-\/]+',
		'зелено-' => 'зелено[-\/]+',
		'красно-' => 'красно[-\/]+',
		'сине-'   => 'сине[-\/]+',
		'серо-'   => 'серо[-\/]+',
		'темно-'  => 'т[емно]*[\.-]+',
		'черно-'  => 'черно[-\/]+',
	];
}
