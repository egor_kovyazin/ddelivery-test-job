<?php

namespace controllers;

use core\Controller;
use models\Items;

/**
 * Контроллер "site"
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
class SiteController extends Controller
{
	const NONE_VALUE = 'none';

	public function actionIndex()
	{
		$itemsOnPage = 10;
		$page = !empty($_GET['page']) ? (int)$_GET['page']: 1;

		$options = [
			'limit'  => $itemsOnPage,
			'offset' => ($page - 1) * $itemsOnPage,
		];

		if (!empty($_GET['items']) && is_array($_GET['items'])) {
			$itemsFilter = $_GET['items'];
			foreach ($itemsFilter as $field => $value) {
				if (empty($value)) {
					continue;
				}
				$options['where'][$field] = ($value == self::NONE_VALUE) ? '': $value;
			}
		}

		list($items, $itemsCount) = Items::getInstance()->selectList($options);

		$this->render('index', [
			'items'       => $items,
			'itemsCount'  => $itemsCount,
			'currentPage' => $page,
			'itemsOnPage' => $itemsOnPage,
		]);
	}

}
