<?php

namespace models;

use core\Fw;
use PDOException;
use PDO;

/**
 * Клас для работы с таблицей "items".
 * @todo Это неполноценная модель, а так :)
 * 
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
class Items
{

	private static $_instance;

	private $_fieldsLike = [
		'model',
		'title'
	];

	/**
	 * Инициализация объекта.
	 * @return \models\Items
	 */
	public static function getInstance()
	{
		if (self::$_instance == null) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * 
	 * @param array $options as [
	 *   'limit'  => <int>,
	 *   'offset' => <int>,
	 *   'where'  => [
	 *     <field name> => <value>,
	 *   ],
	 * ]
	 * @return array as [
	 *   <items list>,
	 *   <count>
	 * ]
	 */
	public function selectList(array $options = [])
	{
		$where = [];
		if (!empty($options['where'])) {

			foreach ($options['where'] as $field => $value) {
				$where[] = $this->prepareSelectedValues($field, $value);
			}
		}

		$limit  = isset($options['limit']) ? (int)$options['limit']: 10;
		$offset = isset($options['offset']) ? (int)$options['offset']: 0;

		$sql = 'SELECT {{fields}} FROM items'
			. (!empty($where) ? ' WHERE ' . join(' AND ', $where): '');

		try {
			return [
				Fw::app()->db->query(str_replace('{{fields}}', '*', $sql) . " LIMIT $offset, $limit")->fetchAll(PDO::FETCH_ASSOC),
				Fw::app()->db->query(str_replace('{{fields}}', 'count(id)', $sql))->fetchColumn(),
			];
		} catch(PDOException $e) {
			Fw::log($e->getMessage());
		}

		return null;
	}

	/**
	 * Обработка значений для выборки.
	 * @param string $field наименвоание поля.
	 * @param mixed $value значение.
	 * @return array
	 */
	private function prepareSelectedValues($field, $value)
	{
		$pdo = Fw::app()->db;

		if (is_array($value)) {
			return $field .' IN ' . join(',', array_map(function($subValue) use ($pdo) {
				return $pdo->quote($subValue);
			}, $value));
		} elseif ($value === null) {
			return $field .' IS NULL';
		} elseif (in_array($field, $this->_fieldsLike)) {
			return $field .' LIKE ' . $pdo->quote($value . '%');
		} else {
			return $field .' = ' . $pdo->quote($value);
		}
	}

	public function getSectionsList()
	{
		return $this->queryListWithCount('section');
	}

	public function getBrandsList()
	{
		return $this->queryListWithCount('brand');
	}

	/**
	 * Строки, сгрупированные по указанному столбцу.
	 * @param string $columnName
	 * @return array as [
	 *   [
	 *     'title'   => <title column>,
	 *     'quantity'=> <quantity items>,
	 *   ],
	 *   ...
	 * ]
	 */
	private function queryListWithCount($columnName)
	{
		try {
			$sql = 'SELECT {{column}} AS title, count(id) AS quantity FROM items GROUP BY {{column}} ORDER BY {{column}} ASC';
			$sql = str_replace('{{column}}', $columnName, $sql);

			return Fw::app()->db->query($sql)->fetchAll();
		} catch(PDOException $e) {
			Fw::log($e->getMessage());
		}

		return [];
	}
}
