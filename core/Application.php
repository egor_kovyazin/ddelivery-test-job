<?php

namespace core;

use Exception;
use PDO;

/**
 * Базовый класс приложений.
 * Инициализирует и выступает в роли контейнера основынх компонентов.
 * 
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * @package core
 * 
 * @property \PDO $db PDO object for database {@link Application::__get()}.
 */
class Application
{
	/**
	 * Контейнер для инициализированных компонентов.
	 * @var array
	 */
	private $_components = [];

	/**
	 * Конструктор.
	 * @param array $config данные для конфигурирования.
	 */
	public function __construct($config = null)
	{
		if (isset($config['components']['db'])) {
			$settings = $config['components']['db'];

			$pdo = new PDO($settings['connectionString'], $settings['username'], $settings['password']);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); // Error Handling

			if (isset($settings['charset'])) {
				$pdo->prepare("SET NAMES :charset")->execute([':charset' => $settings['charset']]);
			}

			$this->_components['db'] = $pdo;
		}
	}

	/**
	 * Возвращает запрашиваемый компонент.
	 * @param string $name наименование компонента.
	 * @return mixed
	 * @throws Exception если компонент не найден.
	 */
	public function __get($name)
	{
		if (isset($this->_components[$name])) {
			return $this->_components[$name];
		}

		throw new Exception("Property \"$name\" not exists.");
	}

	/**
	 * Метод инициализации приложения.
	 */
	public function run()
	{
		register_shutdown_function([$this, 'end'], 0, false);
		set_exception_handler([$this, 'exceptionHandler']);
	}

	/**
	 * Обработчик для неперехваченных исключений.
	 * @param Exception $exception
	 */
	public function exceptionHandler(Exception $exception)
	{
		restore_error_handler();
		restore_exception_handler();

		echo '<h1>'.get_class($exception)."</h1>\n";
		echo '<p>'.$exception->getMessage().' ('.$exception->getFile().':'.$exception->getLine().')</p>';
		echo '<pre>'.$exception->getTraceAsString().'</pre>';

		$this->end(1);
	}

	/**
	 * Метод завершения приложения.
	 * @param integer $status код завершения приложения.
	 * @param integer $exit флаг завершения.
	 */
	public function end($status = 0, $exit = true)
	{
		if ($exit) {
			exit($status);
		}
	}
}
