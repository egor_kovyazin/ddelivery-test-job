<?php
/**
 * Загрузчик классов.
 * 
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * @package core
 */
class ClassLoader
{
	private static $_classMapAuthoritative = [];

	/**
	 * Загрузка файлов с классами.
	 * @param string $class наименвоание класса.
	 * @return boolean TRUE в случае успешной загрузки.
	 */
	public static function loadClass($class)
	{
		if ($file = self::findFile($class)) {
			include($file);
			self::$_classMapAuthoritative[] = $class;
			return true;
		}

		return false;
	}

	/**
	 * Поиск файла с классом.
	 * @param string $class
	 * @return string|boolean путь к файлу.
	 */
	protected static function findFile($class)
	{
		if (in_array($class, self::$_classMapAuthoritative)) {
			return false;
		}

		$basePath = __DIR__ . '/..';

		// include class file relying on include_path
		if(strpos($class,'\\') === false) { // class without namespace
			$className = $class;
		} else { // class name with namespace in PHP 5.3
			$className =  str_replace('\\','/',ltrim($class,'\\'));
		}

		$file = $basePath . DIRECTORY_SEPARATOR . $className . '.php';

		return $file;
	}
}
